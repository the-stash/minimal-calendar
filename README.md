# Minimal Calendar

This is a mod for Calendar/Weather by DasSauerkraut, and thus depends on it. Minimal Calendar/Weather transforms the original mod's UI into an opinionated, minimal version of it without the clock and weather.

![Animation displaying Minimal Calendar/Weather in action](Minimal Calendar.gif)